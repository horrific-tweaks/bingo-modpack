FROM golang:1.22.3-alpine3.19 AS builder

RUN go install github.com/packwiz/packwiz@9cca74476da41dafe85e04090d979921b55d6086

COPY quilt /home/root/quilt

WORKDIR /home/root/quilt

RUN packwiz modrinth export
RUN packwiz curseforge export

# Export only the resulting .mrpack file
FROM scratch
COPY --from=builder /home/root/quilt/*.mrpack /
COPY --from=builder /home/root/quilt/*.zip /
