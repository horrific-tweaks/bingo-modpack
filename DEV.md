You can open this project using [devcontainers](https://code.visualstudio.com/docs/devcontainers/tutorial) for simplified installation/setup. This will install [packwiz](https://packwiz.infra.link) for you.

See the [packwiz documentation](https://packwiz.infra.link/tutorials/creating/getting-started/) for a list of commands to add or update mods in this modpack.

To build and export the pack, use `docker build -o dist .`

## Config Options

### Roughly Enough Items (`config/roughlyenoughitems/config.json5`)

- `appearance.hideEntryPanelIfIdle: true`
- `advanced.miscellaneous.clickableRecipeArrows: false`
- `advanced.miscellaneous.categorySettings.hiddenCategories: [minecraft:plugins/tag]`

### Sodium Extra (`config/sodium-extra-options.json`)

- `render_settings.multi_dimension_fog_control: true`
- `render_settings.dimensionFogDistance["minecraft:the_nether"]: 33` (no fog)
- `extra_settings.tutorial_toast: false`
