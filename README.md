![Yet Another Bingo Ultimate](https://gitlab.com/horrific-tweaks/bingo-modpack/-/raw/main/docs/banner.png)

*For an introduction to Minecraft BINGO, read "[What is Minecraft BINGO?](https://minecraft.horrific.dev/bingo/what-is-bingo)" on the wiki!*

This modpack is built around [Yet Another BINGO](https://modrinth.com/mod/yet-another-minecraft-bingo), a multiplayer item hunt mod. It includes everything needed for the bingo mod as well as some quality-of-life additions: Zoomify, Dynamic Lights, Roughly Enough Items, Coords HUD, and more!

<table>
<tbody>
<tr>
<td>
<img alt="A screenshot of Minecraft game with a BINGO card in the corner" src="https://gitlab.com/horrific-tweaks/bingo/-/raw/main/docs/screenshot-with-hud-3.png" />
</td>
<td>

Up to 8 teams can compete to collect items on the BINGO card. Score items in horizontal, vertical, or diagonal
lines to win the game.

---

⌨ Press "Y" to see the Bingo Card in an inventory view!

</td>
</tr>
</tbody>
</table>

## ⌨️ Keybinds

Here are a few useful keys that you might want to know:

- <kbd>Y</kbd> - View the BINGO card
- <kbd>C</kbd> - Zoom
- <kbd>Tab</kbd> - Show scoreboard

## Included Mods

This is not a complete list - see the [versions](https://modrinth.com/project/yet-another-bingo-ultimate/versions) tab for all of the mods and libraries used in the pack. I've limited this list to things that you're likely to notice as a player:

- [C2ME](https://modrinth.com/mod/c2me-fabric)
- [Coords HUD](https://modrinth.com/mod/coords-hud)
- [More Chat History](https://modrinth.com/mod/morechathistory)
- [Ok Zoomer](https://modrinth.com/mod/ok-zoomer)
- [Roughly Enough Items](https://modrinth.com/mod/rei)
- [LambDynamicLights](https://modrinth.com/mod/lambdynamiclights)
- [Scoreboard Overhaul](https://modrinth.com/mod/scoreboard-overhaul)
- [Sodium](https://modrinth.com/mod/sodium) + [Sodium Extra](https://modrinth.com/mod/sodium-extra) + [Iris](https://modrinth.com/mod/iris)
- [Vanilla Tweaks](https://vanillatweaks.net)<br>
  Visual Waxed Copper, Honey Stages, Unique Dyes, Unobtrusive Rain/Snow, Lower Shield
- [Yet Another BINGO](https://modrinth.com/mod/yet-another-minecraft-bingo)

## Credits

The very pretty Bingo Card HUD and "Bingo!" button textures were made by [@marceles](https://marceles.carrd.co)!

Parts of this mod are inspired from [Extremelyd1/minecraft-bingo](https://github.com/Extremelyd1/minecraft-bingo), the awesome Spigot/Paper version of this gamemode.

The logo design was inspired by [Aikoyori/ProgrammingVTuberLogos](https://github.com/Aikoyori/ProgrammingVTuberLogos)

Vanilla Tweaks: https://vanillatweaks.net/<br>
(in compliance with the terms and conditions at https://vanillatweaks.net/terms/)
